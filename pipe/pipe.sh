#!/usr/bin/env bash
#
# This pipe is created for leaning purpose.
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
NAME=${NAME:?'NAME variable missing.'}

# Default parameters
DEBUG=${DEBUG:="false"}

run echo "Namaste ${NAME}"

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi
